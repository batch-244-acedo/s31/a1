/* 

- What directive is used by Node.js in loading the modules it needs?
ans: require

- What Node.js module contains a method for server creation?
ans: http - Hypertext Transfer Protocol

- What is the method of the http object responsible for creating a server using Node.js?
ans: createServer()

- What method of the response object allows us to set status codes and content types?
ans: writeHead()

- Where will console.log() output its contents when run in Node.js?
ans: console(GitBash)

- What property of the request object contains the address's endpoint?
ans: response.end

*/

const http = require("http");

const port = 3000;

const server = http.createServer((request, response) => {
    if(request.url == "/login"){
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Welcome to the login page.");
    } else {
        response.writeHead(404, {"Content-Type": "text/plain"});
        response.end("I'm sorry the page you are looking for cannot be found");
    }

});

server.listen(port);

console.log(`Server is now running at localhost: ${port}`);